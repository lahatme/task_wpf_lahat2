﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace TaskWPF.Infrastructure
{
    public class CommandHandler : ICommand
    {
        Action _execute;
        Func<bool> _canExecute;


        public CommandHandler(Action execute, Func<bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            {
                return _canExecute();
            }
            else
            {
                return false;
            }
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _execute();
        }
    }

}
