﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Controls;
using TaskWPF.Service;
using TaskWPF.Models;


namespace TaskWPF.BusinessLogic
{
    public class TaskBusinessLogic
    {
        private static readonly object lockThis = new object();

        private static TaskBusinessLogic instance = null;
        public static TaskBusinessLogic GetInstance
        {
            get
            {
                lock (lockThis)
                {
                    if (instance == null)
                    {
                        instance = new TaskBusinessLogic();
                    }
                    return instance;
                }
            }
        }
        public CsvParser _csvParser;
        public MailService _mailService;
        public TaskBusinessLogic()
        {
            _csvParser = new CsvParser();
            _mailService = new MailService();
        }

        public DataTable GetTable(string file)
        {
            DataTable table = new DataTable();
            table = _csvParser.ParseCsv(file, "lahat");
            return table;
        }

        public void SendMail(MailTab mailTab)
        {
            _mailService.SendMail(mailTab);
        }
    }
}
