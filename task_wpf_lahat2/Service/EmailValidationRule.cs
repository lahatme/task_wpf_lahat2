﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;

namespace TaskWPF.Service
{
    public class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            string valueInString = value.ToString();
            if (string.IsNullOrWhiteSpace(valueInString) == true)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            try
            {
                //Check if the email is in email format
                var addr = new System.Net.Mail.MailAddress(valueInString);
                if ((addr.Address == valueInString) == false)
                {
                    return new ValidationResult(false, "Invalid email address");
                }
            }
            catch
            {
                return new ValidationResult(false, "Invalid email address");
            }
            return new ValidationResult(true, "");

        }
    }
}
