﻿using System;
using System.Collections.Generic;
using TaskWPF.Models;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows;

namespace TaskWPF.Service
{
    public class MailService
    {
        public MailService()
        {

        }
        public void SendMail(MailTab mailTab)
        {
            string smtpServerName = "smtp.gmail.com";
            int port = 587;
            string senderEmailId = mailTab.From;
            string senderPassword = mailTab.Password;
            string to = mailTab.To;
            string subject = mailTab.Subject;
            string body = mailTab.Body;
            SmtpClient smptClient = new SmtpClient(smtpServerName, Convert.ToInt32(port))
            {
                Credentials = new NetworkCredential(senderEmailId, senderPassword),
                EnableSsl = true
            };
            smptClient.UseDefaultCredentials = true;
            try
            {
                smptClient.Send(senderEmailId, to, subject, body);
                MessageBox.Show("Message Sent Successfully");
            }
            catch
            {
                MessageBox.Show("Sending the message failed");

            }
        }
    }
}
