﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;

namespace TaskWPF.Service
{
    public class TextValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            string valueInString = value.ToString();
            if (string.IsNullOrWhiteSpace(valueInString) == true)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
           
            return new ValidationResult(true, "");
        }
    }
}
