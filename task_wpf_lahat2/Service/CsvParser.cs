﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TaskWPF.Service
{
    public class CsvParser
    {
        public DataTable ParseCsv(string file, string tableName)
        {
            //The DataSet to Return
            DataSet result = new DataSet();

            //Open the file in a stream reader.
            StreamReader s = new StreamReader(file);

            //Split the first line into the columns       
            string regex = ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
            Regex fieldsRegex = new Regex(regex);

            string columnsLine = s.ReadLine();

            var columns = fieldsRegex.Split(columnsLine);
            RemoveQuoatsAndSpaces(columns);

            //Add the new DataTable to the RecordSet
            result.Tables.Add(tableName);

            //Cycle the colums, adding those that don't exist yet 
            //and sequencing the one that do.
            foreach (var col in columns)
            {
                var added = false;
                var next = "";
                var i = 0;
                while (!added)
                {
                    //Build the column name and remove any unwanted characters.
                    string columnName = col + next;
                    DataColumnCollection column = result.Tables[tableName].Columns;
                    //See if the column already exists
                    if (!column.Contains(columnName))
                    {
                        //if it doesn't then we add it here and mark it as added
                        column.Add(columnName);
                        added = true;
                    }
                    else
                    {
                        //if it did exist then we increment the sequencer and try again.
                        i++;
                        next = "_" + i.ToString();
                    }
                }
            }

            //Read the rest of the data in the file.        
            string allData = s.ReadToEnd();

            //Split off each row at the Carriage Return/Line Feed
            //Default line ending in most windows exports.  
            //You may have to edit this to match your particular file.
            //This will work for Excel, Access, etc. default exports.
            string[] rows = allData.Split("\n".ToCharArray());

            //Now add each row to the DataSet        
            foreach (string r in rows)
            {
                //Split the row at the delimiter.
                string[] items = fieldsRegex.Split(r);

                //Add the item
                result.Tables[tableName].Rows.Add(items);
            }

            //Return the imported data. 
            var currentTable = result.Tables[0];
            if (currentTable != null)
                return currentTable.DefaultView.ToTable();
            else
            {
                return null;
            }

        }



        private void RemoveQuoatsAndSpaces(string[] columns)
        {
            if (columns != null)
            {
                for (var i = 0; i < columns.Length; i++)
                {
                    if (columns[i].StartsWith("\""))
                    {
                        columns[i] = columns[i].Substring(1);
                    }

                    if (columns[i].EndsWith("\""))
                    {
                        columns[i] = columns[i].Remove(columns[i].Length - 1, 1);
                    }
                }
            }
        }
    }
}
