﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;

namespace TaskWPF.Service
{
    public class AgeValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value == null) {
                return new ValidationResult(false, "The value cannot be empty");
            }
            string valueInString = value.ToString();
            if (string.IsNullOrWhiteSpace(valueInString) == true)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            int valueToInt;
            try
            {
                 valueToInt = Int32.Parse(valueInString);
            }
            catch
            {
                return new ValidationResult(false, "The value must be an integer");
            }
           
            if(valueToInt < 0 || valueToInt > 120)
            {
                return new ValidationResult(false, "The value must be a number between 0 and 120");

            }
            return new ValidationResult(true, "");
        }
    }
}
