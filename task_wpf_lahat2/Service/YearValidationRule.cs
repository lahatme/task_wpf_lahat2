﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;

namespace TaskWPF.Service
{
    public class YearValidationRule : ValidationRule
    {


        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            string valueInString = value.ToString();
            if (string.IsNullOrWhiteSpace(valueInString) == true)
            {
                return new ValidationResult(false, "The value cannot be empty");
            }
            int valueToInt;
            try
            {
                valueToInt = Int32.Parse(valueInString);
            }
            catch
            {
                return new ValidationResult(false, "The value must be an integer");
            }

            if (valueToInt < 1900 || valueToInt > 2020)
            {
                return new ValidationResult(false, "The value must be a number between 1900 and 2020");

            }
            return new ValidationResult(true, "");

        }
    }
}
