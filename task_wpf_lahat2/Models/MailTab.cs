﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskWPF.Models
{
    public class MailTab
    {
        public string From;
        public string To;
        public string Password;
        public string Subject;
        public string Body;
    }
}
