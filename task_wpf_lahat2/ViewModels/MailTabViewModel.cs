﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Resources;
using System.Text;
using System.Windows;
using TaskWPF.Models;
using System.Windows.Input;
using TaskWPF.BusinessLogic;
using TaskWPF.Infrastructure;
using System.Threading.Tasks;

namespace TaskWPF.ViewModels
{
    public class MailTabViewModel : BasicTabViewModel
    {
        public TaskBusinessLogic _businessLogic;
        public string From { get; set; }
        public string To { get; set; }
        public string Password { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MailTabViewModel(string name):base(name) {
            _businessLogic = TaskBusinessLogic.GetInstance;
        }

        private int _age;
        public int Age
        {
            get { return _age; }
            set
            {
                _age = value;
            }
        }

        private int _year;
        public int Year
        {
            get { return _year; }
            set
            {
                _year = value;
            }
        }


        private ICommand _sendMailCommand;
        public ICommand SendMailCommand
        {
            get
            {
                return _sendMailCommand ?? (_sendMailCommand = new CommandHandler(() => SendMail(), () => true));
            }
        }

        public void SendMail()
        {
            if (IsValid() == true) {
                MailTab mailTab = new MailTab();
                mailTab.Body = Body;
                mailTab.From = From;
                mailTab.To = To;
                mailTab.Password = Password;
                mailTab.Subject = Subject;
                _businessLogic.SendMail(mailTab);
            }
        }
        public bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(Body) == true)
            {
                MessageBox.Show("Body is empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(To) == true)
            {
                MessageBox.Show("To is empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(From) == true)
            {
                MessageBox.Show("From is empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Subject) == true)
            {
                MessageBox.Show("Subject is empty");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Password) == true)
            {
                MessageBox.Show("Password is empty");
                return false;
            }
            try
            {
                //Check if the email is in email format
                var addr = new System.Net.Mail.MailAddress(From);
                if ((addr.Address == From) == false)
                {
                    MessageBox.Show("From is invalid");
                    return false;
                }
            }
            catch
            {
                MessageBox.Show("From is invalid");
                return false;
            }
            try
            {
                //Check if the email is in email format
                var addr = new System.Net.Mail.MailAddress(To);
                if ((addr.Address == To) == false)
                {
                    MessageBox.Show("To is invalid");
                    return false;
                }
            }
            catch
            {
                MessageBox.Show("To is invalid");
                return false;
            }
            return true;

        }
    }
}
