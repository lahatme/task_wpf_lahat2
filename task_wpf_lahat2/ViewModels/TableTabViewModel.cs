﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TaskWPF.Infrastructure;
using TaskWPF.BusinessLogic;
using System.Data;
using System.Windows;
using System.IO;
using System.ComponentModel;

namespace TaskWPF.ViewModels
{
    public class TableTabViewModel : BasicTabViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public TaskBusinessLogic _businessLogic;
        private DataTable _table;
        public DataTable Table
        {
            get { return _table; }
            set
            {
                if (_table != value)
                    _table = value;
                OnPropertyChanged(nameof(Table));
            }
        }

        public TableTabViewModel(string name) : base(name)
        {
            _businessLogic = TaskBusinessLogic.GetInstance;
        }

        public TableTabViewModel(string name, DataTable _table) : base(name)
        {
            _businessLogic = TaskBusinessLogic.GetInstance;
            Table = _table;
        }

        protected void OnPropertyChanged(string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
