﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Windows;
using TaskWPF.Infrastructure;
using System.Data;
using TaskWPF.BusinessLogic;
using System.ComponentModel;

namespace TaskWPF.ViewModels
{
    public class MainWindowViewModel : Window
    {

        public ObservableCollection<BasicTabViewModel> Tabs
        {
            get;
            set;
        }

        public MainWindowViewModel()
        {
            _businessLogic = TaskBusinessLogic.GetInstance;
            Tabs = new ObservableCollection<BasicTabViewModel>();
        }
        private BasicTabViewModel _selectedTab;
        public BasicTabViewModel SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                if (_selectedTab != value)
                    _selectedTab = value;
                OnPropertyChanged(nameof(SelectedTab));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public int GetTabsCount()
        {
            return Tabs.Count;
        }
        private ICommand _addMailTabCommand;
        public ICommand AddMailTabCommand
        {
            get
            {
                return _addMailTabCommand ?? (_addMailTabCommand = new CommandHandler(() => AddMailTab(), () => true));
            }
        }

        public void AddMailTab()
        {
            Tabs.Add(new MailTabViewModel("Mail Tab"));
        }

        private ICommand _addTableTabCommand;
        public ICommand AddTableTabCommand
        {
            get
            {
                return _addTableTabCommand ?? (_addTableTabCommand = new CommandHandler(() => AddTableTab(), () => true));
            }
        }
        public TaskBusinessLogic _businessLogic;

        public DataTable Table
        {
            get;
            set;
        }


        public void AddTableTab()
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "CSV document(*.csv)|*.csv";

            var result = dialog.ShowDialog();

            if (result == true)
            {
                string csvFile = dialog.FileName;
                if (IsValid(csvFile) == true)
                {
                    Table = new DataTable();
                    Table = _businessLogic.GetTable(csvFile);
                }
            }
            Tabs.Add(new TableTabViewModel("Table Tab", Table));
        }

        public bool IsValid(string csvFile)
        {
            if (String.IsNullOrWhiteSpace(csvFile) == true)
            {
                MessageBox.Show("File is null or white space");
                return false;
            }
            if (File.Exists(csvFile) == false)
            {
                MessageBox.Show("File not found");
                return false;
            }
            string ext = Path.GetExtension(csvFile);
            if (string.Equals(ext, ".csv", StringComparison.OrdinalIgnoreCase) == false)
            {
                MessageBox.Show("File not a csv file");
                return false;
            }
            return true;
        }
    }
}
