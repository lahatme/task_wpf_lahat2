﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskWPF.ViewModels
{
    public class BasicTabViewModel
    {
        public string TabName { get; set; }
        public BasicTabViewModel(string name)
        {
            TabName = name;
        }
    }
}
