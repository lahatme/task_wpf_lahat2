using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using System.Threading;
using System.Windows;
using TaskWPF.ViewModels;

namespace MainWindowUnitTest
{
    [TestClass]
    public class MainWindowTest
    {
        MainWindowViewModel mainWindow;

        [TestInitialize]
        public void Init()
        {

        }

        [TestMethod]
        public void AddMailTabTest()
        {
            MainWindowViewModel window = null;

            // The dispatcher thread
            var t = new Thread(() =>
            {
                window = new MainWindowViewModel();
                int tabsCount = window.GetTabsCount();
                window.AddMailTab();
                Assert.AreEqual(window.GetTabsCount(), tabsCount + 1);
            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();


        }


        [TestMethod]
        public void AddTableTabTest()
        {
            MainWindowViewModel window = null;

            // The dispatcher thread
            var t = new Thread(() =>
            {
                window = new MainWindowViewModel();
                int tabsCount = window.GetTabsCount();
                window.AddTableTab();
                Assert.AreEqual(window.GetTabsCount(), tabsCount + 1);
            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

        }

        [TestMethod]
        public void TestIsValid()
        {
            MainWindowViewModel window = null;

            var t = new Thread(() =>
            {
                window = new MainWindowViewModel();
                OpenFileDialog dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();
                string csvFile = dialog.FileName;
                bool IsValid=false;
                try
                {
                     IsValid = window.IsValid(csvFile);
                }
                catch
                {
                    Assert.Fail();
                }
                Assert.IsTrue(IsValid);
            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }

    }
}
