using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskWPF.ViewModels;

namespace MailTabUnitTest
{
    [TestClass]
    public class MailTabTest
    {
            MailTabViewModel mailTabViewModel;

            [TestInitialize]
            public void Init()
            {
            mailTabViewModel = new MailTabViewModel("MailTab");
            mailTabViewModel.From = "lahat.menashko@mdclone.com";
            mailTabViewModel.To = "lahat.menashko@mdclone.com";
            mailTabViewModel.Password = "123456";
            mailTabViewModel.Body = "ghh";
            mailTabViewModel.Subject = "fghf";
        }

            [TestMethod]
            public void TestIsValid()
            {
            bool IsValid = mailTabViewModel.IsValid();
                Assert.IsTrue(IsValid);
            }

    }
}
