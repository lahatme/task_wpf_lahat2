using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using System.Data;
using System.Threading;
using System.Windows;
using TaskWPF.Service;

namespace CsvParserUnitTest
{
    [TestClass]
    public class TestCsvParser
    {
        CsvParser csvParser;

        [TestInitialize]
        public void Init()
        {
            csvParser = new CsvParser();
        }

        [TestMethod]
        public void DataTableNotNull()
        {
            var t = new Thread(() =>
            {
                OpenFileDialog dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();
                string csvFile = dialog.FileName;
                DataTable dataTable = csvParser.ParseCsv(csvFile, "lahat");
                Assert.IsNotNull(dataTable);

            });

            // Configure the thread
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

        }
    }
}
